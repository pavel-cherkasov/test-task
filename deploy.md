## About deploy
<p>Step to deploy Project:</p>
<ul>
<li>Clone project from repo</li>
<li>Run <code>composer install</code></li>
<li>Run <code>npm install</code> and <code>npm run dev</code></li>
<li>Set up DataBase settings in <code>.env</code> file</li>
<li>run console migration from your server (or container) <code>php artisan migrate</code></li>
<li>Open main page (<code>yor_domen/</code>) from browser</li>
<li>Add new comments and reply</li>
</ul> 

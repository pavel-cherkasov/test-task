<blockquote class="blockquote">
    <p class="m-2 h2">ID: {{$comment->id}}</p>
    <p class="m-2">{{$comment->text}}</p>
    <footer class="blockquote-footer">
        <cite
            title="{{$comment->created_at->format('d/m/Y')}}">{{$comment->created_at->format('d/m/Y m:h')}}</cite>
        <button type="button" class="btn btn-link btn-sm" onclick="setParentId({{$comment->id}})" data-toggle="modal" data-target="#Modal">Reply</button>
    </footer>
</blockquote>


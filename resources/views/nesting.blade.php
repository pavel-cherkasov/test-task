@include('comment', ['comment'=>$data])
@if (count($data->nesting) != 0)
<ul>
    @foreach($data->nesting as $comment)
        @include('nesting', ['data'=>$comment])
    @endforeach
</ul>
@endif

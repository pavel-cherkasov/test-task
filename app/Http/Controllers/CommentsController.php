<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data = Comment::all();
        return view('index', ['data' => $data]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $data = $request->except('_token');
        $coment = new Comment();
        foreach ($data as $key => $value) {
            $coment->{$key} = $value;
        }
        if($coment->save()){
            return redirect()->back()->with('success', ['You add the comment']);
        }else{
            return redirect()->back()->with('errors', ['Something went wrong, the comment was not saved']);
        }
    }
}
